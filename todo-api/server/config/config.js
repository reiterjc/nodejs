var env = process.env.NODE_ENV || 'development';
console.log(`setting configuration enviroment to ${env}`);

if (env === 'development' || env === 'test') {
    var envConfig = require('./config.json')[env];
    Object.keys(envConfig).forEach((key) => {
        process.env[key] = envConfig[key];
    })
}