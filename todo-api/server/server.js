require('./config/config');

const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const { ObjectID } = require('mongodb');

var { mongoose } = require('./db/mongoose');
var { Todo } = require('./models/todo');
var { User } = require('./models/user');
var { authenticate } = require('./middleware/authenticate');

var app = express();

app.use(bodyParser.json());

// create todo
app.post('/todos', authenticate, async (request, response) => {
    var todo = new Todo({
        text: request.body.text,
        _creator: request.user._id
    });

    try {
        let doc = await todo.save();
        response.send(doc);
    } catch (e) {
        response.status(400).send(e);
    };
});

// read todo
app.get('/todos', authenticate, async (request, response) => {
    try {
        let todos = await Todo.find({ _creator: request.user._id });
        response.send({ todos });
    } catch (e) {
        response.status(400).send(e);
    }
});

app.get('/todos/:id', authenticate, async (request, response) => {
    if (!ObjectID.isValid(request.params.id)) {
        return response.status(404).send();
    }

    try {
        let todo = await Todo.findOne({
            _id: request.params.id,
            _creator: request.user._id
        });

        if (!todo) {
            return response.status(404).send();
        }
        response.send({ todo });
    } catch (e) {
        response.status(400).send(e);
    }
});

// update todo
app.patch('/todos/:id', authenticate, async (request, response) => {
    var id = request.params.id;
    var body = _.pick(request.body, ['text', 'completed']);

    if (!ObjectID.isValid(request.params.id)) {
        return response.status(404).send();
    }

    if (_.isBoolean(body.completed) && body.completed) {
        body.completedAt = new Date().getTime();
    } else {
        body.completed = false;
        body.completedAt = null;
    }

    try {
        let todo = await Todo.findOneAndUpdate({ _id: id, _creator: request.user._id }, { $set: body }, { new: true });
        if (!todo) {
            return response.status(404).send();
        }
        response.send({ todo });
    } catch (e) {
        response.status(400).send();
    }

});

// delete todo
app.delete('/todos/:id', authenticate, async (request, response) => {
    if (!ObjectID.isValid(request.params.id)) {
        return response.status(404).send();
    }

    try {
        let todo = await Todo.findOneAndRemove({
            _id: request.params.id,
            _creator: request.user._id
        });

        if (!todo) {
            return response.status(404).send();
        }
        response.send({ todo });
    } catch (e) {
        response.status(400).send(e);
    }
});

// create user
app.post('/users', async (request, response) => {
    var body = _.pick(request.body, ['email', 'password']);
    var user = new User(body);

    try {
        await user.save();
        let token = await user.generateAuthToken();
        response.header('x-auth', token).send(user);
    } catch (e) {
        response.status(400).send(e);
    }
});

app.get('/users/me', authenticate, (request, response) => {
    response.send(request.user);
});

app.post('/users/login', async (request, response) => {
    var body = _.pick(request.body, ['email', 'password']);

    try {
        let user = await User.findByCredentials(body.email, body.password);
        let token = await user.generateAuthToken();
        response.header('x-auth', token).send(user);
    } catch (e) {
        response.status(400).send();
    }
});

// logout
app.delete('/users/me/token', authenticate, async (request, response) => {
    try {
        await request.user.removeToken(request.token);
        response.status(200).send();
    } catch (e) {
        response.status(400).send();
    }
});

app.listen(process.env.PORT, () => {
    console.log(`Started on port ${process.env.PORT}`);
});

module.exports = {
    app
}