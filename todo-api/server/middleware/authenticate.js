var { User } = require('./../models/user')

var authenticate = async (request, response, next) => {
    var token = request.header('x-auth');

    try {
        let user = await User.findByToken(token);
        if (!user) {
            return Promise.reject();
        }

        request.user = user;
        request.token = token;
        next();
    } catch (e) {
        response.status(401).send(e);
    }
};

module.exports = {
    authenticate
};