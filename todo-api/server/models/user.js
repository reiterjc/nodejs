const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

var UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        minlength: 1,
        trim: true,
        unique: true,
        validate: {
            validator: validator.isEmail,
            message: '{VALUE} is not a valid email'
        }
    },
    password: {
        type: String,
        required: true,
        minlength: 6
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            type: String,
            required: true
        }
    }]
});

UserSchema.methods.toJSON = function () {
    var user = this;
    var userObject = user.toObject();

    return _.pick(userObject, ['_id', 'email']);
};

UserSchema.methods.generateAuthToken = async function () {
    var user = this;
    var access = 'auth';
    var token = jwt.sign({ _id: user._id.toHexString(), access }, process.env.JWT_SECRET).toString();
    user.tokens = user.tokens.concat([{ access, token }]);
    await user.save();
    return token;
};

UserSchema.methods.removeToken = function (token) {
    var user = this;

    return user.update({
        $pull: {
            tokens: { token }
        }
    })
};

UserSchema.statics.findByToken = function (token) {
    var User = this;

    var decoded;
    try {
        decoded = jwt.verify(token, process.env.JWT_SECRET);
    } catch (e) {
        return Promise.reject();
    }

    return User.findOne({
        '_id': decoded._id,
        'tokens.token': token,
        'tokens.access': 'auth'
    });
};

UserSchema.statics.findByCredentials = async function (email, password) {
    var User = this;

    let user = await User.findOne({ email });
    if (!user) {
        return;
    }

    try {
        let result = await bcrypt.compare(password, user.password);
        if (!result) {
            throw new Error('Passwords do not match');
        }
        return user;
    } catch (e) {
        throw new Error('Error comparing encryped password');
    }
};

UserSchema.pre('save', async function hashPassword(next) {

    var user = this;

    if (!user.isModified('password')) return next();
    
    try {
        let salt = await bcrypt.genSalt(10);
        let hash = await bcrypt.hash(user.password, salt);
        user.password = hash;

        return next();
    } catch (e) {
        return next(e);
    }
});

var User = mongoose.model('User', UserSchema);

module.exports = {
    User
}