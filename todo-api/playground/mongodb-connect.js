const {MongoClient, ObjectID} = require('mongodb');

var obj = new ObjectID();
console.log(obj);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (error, client) => {
    if (error) {
        return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');
    const db = client.db('TodoApp');

    db.collection('Todos').insertOne({
        text: 'Eat lunch',
        completed: false
    }, (error, result) => {
        if (error) {
            return console.log('Unable to insert todo', error);
        }

        console.log(JSON.stringify(result.ops, undefined, 2));
    });

    // db.collection('Users').insertOne({
    //         name: 'Justin',
    //         age: 37,
    //         location: 'Madison'
    //     }, (error, result) => {
    //         if (error) {
    //             return console.log('Unable to insert user', error);
    //         }
    
    //         console.log(JSON.stringify(result.ops, undefined, 2));
    // });

    client.close();
});