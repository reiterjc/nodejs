const express = require('express');

var app = express();

app.get('/', (req, res) => {
    res.send('Hello World');
});

app.get('/notfound', (req, res) => {
    res.status(404).send({
        error: 'Page not found',
        name: 'Todo App'
    });
});

app.listen(3000);

module.exports.app = app;