const request = require('supertest');
const expect = require('expect');

var app = require('./server.js').app;

describe('Server', () => {
    describe('GET /', () => {
        it('should return hello world response', (done) => {
            request(app)
                .get('/')
                .expect(200)
                .expect('Hello World')
                .end(done);
        });
    });

    describe('GET /notfound', () => {
        it('should return 404 status', (done) => {
            request(app)
                .get('/notfound')
                .expect(404)
                .end(done);
        });
    });
});