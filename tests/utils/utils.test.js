const expect = require('expect');

const utils = require ('./utils.js');

describe('Utils', () => {

    describe('#add', () => {
        it('should add two numbers', () => {
            var result = utils.add(33, 11);
            expect(result).toBe(44).toBeA('number');
        });
    });
    
    it('should async add two numbers', (done) => {
        var result = utils.asyncAdd(4, 3, (sum) => {
            expect(sum).toBe(7).toBeA('number');
            done();
        });
    });
    
    it('should square a number', () => {
        var result = utils.square(3);
        expect(result).toBe(9).toBeA('number');
    });
});

it('should contain some values', () => {
    expect({
        name: 'Justin',
        age: 37,
        location: 'Madison'
    }).toInclude({
        age: 37
    })
});

it('should set first and last names', () => {
    var user = {
        age: 37,
        location: 'Madison'
    }
    var result = utils.setName(user, 'Justin Reiter');
    expect(result).toInclude({
        firstName: 'Justin',
        lastName: 'Reiter'
    });
})