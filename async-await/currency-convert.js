const axios = require('axios');

const getExchangeRate = async (from, to) => {
    try {
        let response = await axios.get(`http://api.fixer.io/latest?base=${from}`);
        return response.data.rates[to];
    } catch (e) {
        throw new Error(`Unable to get exhange rate for ${from} and ${to}`);
    }
};

const getContries = async (currencyCode) => {
    try {
        let response = await axios.get(`https://restcountries.eu/rest/v2/currency/${currencyCode}`);
        return response.data.map((country) => country.name);
    } catch (e) {
        throw new Error(`Unable to get the countries that use ${currencyCode}`)
    }
};

const convertCurrency = async (from, to, amount) => {
    let rate = await getExchangeRate(from, to);
    let countryList = await getContries(to);
    let exchangedAmount = amount * rate;

    return `${amount} ${from} is worth ${exchangedAmount} ${to}.  ${to} can be used in the following countries: ${countryList.join(', ')}`;
};

convertCurrency('CAD', 'USD', 100).then((status) => {
    console.log(status);
}).catch((e) => {
    console.log(e.message);
});