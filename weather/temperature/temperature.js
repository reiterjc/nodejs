const request = require('request');

var currentTemperature = (latitude, longitude, callback) => {
    
    request({
        url: `https://api.darksky.net/forecast/3657d0add016e8c631540b6222b7001f/${latitude},${longitude}`,
        json: true
    }, (error, response, body) => {
        if (!error && response.statusCode === 200) {
            callback(undefined, {
                currentTemperature: body.currently.temperature,
                apparentTemperature: body.currently.apparentTemperature
            });
        } else {
            callback('Unable to fetch weather');
        }
    });
}

module.exports = {
    currentTemperature
};