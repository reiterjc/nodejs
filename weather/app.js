const yargs = require('yargs');

const geocode = require('./geocode/geocode');
const temperature = require('./temperature/temperature');

const argv = yargs
    .options({
        a: {
            demand: true,
            alias: 'address',
            describe: 'Address to fetch weather for',
            string: true
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

geocode.geocodeAddress(argv.address, (errorMessage, geocodeResults) => {
    if (errorMessage) {
        console.log(errorMessage);
    } else {
        temperature.currentTemperature(geocodeResults.latitude, geocodeResults.longitude, (errorMessage, weatherResults) => {
            if (errorMessage) {
                console.log(errorMessage);
            } else {
                console.log(`It is currently ${weatherResults.currentTemperature}.  It feels like ${weatherResults.apparentTemperature}`);
            }
        });
    }
});